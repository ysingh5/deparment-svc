package com.altimetrik.deparment.svc.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.deparment.svc.dto.DepartmentDTO;
import com.altimetrik.deparment.svc.service.DepartmentService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/depsvc")
@AllArgsConstructor
@Slf4j
public class DepartmentController {

	private DepartmentService departmentService;
	
	@PostMapping
	public DepartmentDTO doSave(@RequestBody DepartmentDTO dto){
		log.info("Req: Department:{}", dto.getName());
		return departmentService.doSave(dto);
	}
	
	@GetMapping("{code}")
	public DepartmentDTO getByCode(@PathVariable String code){
		log.info("Req: Department code:{}", code);
		return departmentService.getByCode(code);
	}
	
	@GetMapping
	public List<DepartmentDTO> getAll(){
		log.info("Req: Department");
		return departmentService.getAll();
	}	
	
}
