package com.altimetrik.deparment.svc.utills;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.altimetrik.deparment.svc.dto.DepartmentDTO;
import com.altimetrik.deparment.svc.entity.Department;

@Mapper
public interface ObjectMapper {
  
	ObjectMapper MAPPER = Mappers.getMapper(ObjectMapper.class);
	//@Mapping()
	DepartmentDTO mapToDepartmentDTO(Department department);
	
	Department mapToDepartment(DepartmentDTO dto);
}
