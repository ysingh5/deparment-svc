package com.altimetrik.deparment.svc.service.impl;

import java.rmi.AlreadyBoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.altimetrik.deparment.svc.dto.DepartmentDTO;
import com.altimetrik.deparment.svc.entity.Department;
import com.altimetrik.deparment.svc.exception.DataAlreadyExistsException;
import com.altimetrik.deparment.svc.exception.ResourceNotFoundException;
import com.altimetrik.deparment.svc.repository.DepartmentRepo;
import com.altimetrik.deparment.svc.service.DepartmentService;
import com.altimetrik.deparment.svc.utills.ObjectMapper;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

	private DepartmentRepo departmentRepo;

	@Override
	public DepartmentDTO doSave(DepartmentDTO dto) {
		
		Optional<Department> dbData = departmentRepo.findByCode(dto.getCode());
		if(dbData.isPresent()) {
			throw new DataAlreadyExistsException("Department code already exists.");
		}
		
		Department dep = ObjectMapper.MAPPER.mapToDepartment(dto);
		Department saveDep = departmentRepo.save(dep);
		return ObjectMapper.MAPPER.mapToDepartmentDTO(saveDep);
	}

	@Override
	public DepartmentDTO getByCode(String code) {
		Department dep = departmentRepo.findByCode(code)
				.orElseThrow(() -> new ResourceNotFoundException("Department", "code", code));
		return ObjectMapper.MAPPER.mapToDepartmentDTO(dep);
	}

	@Override
	public List<DepartmentDTO> getAll() {

		List<Department> list = departmentRepo.findAll();
		if (null != list) {
			return list.stream().map(department -> ObjectMapper.MAPPER.mapToDepartmentDTO(department))
					.collect(Collectors.toList());
		}
		return new ArrayList<>();

	}

}
