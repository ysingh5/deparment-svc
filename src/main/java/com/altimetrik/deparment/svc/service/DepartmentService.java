package com.altimetrik.deparment.svc.service;

import java.util.List;

import com.altimetrik.deparment.svc.dto.DepartmentDTO;

public interface DepartmentService  {
   DepartmentDTO doSave(DepartmentDTO dto);
   
   DepartmentDTO getByCode(String code);
   
   List<DepartmentDTO> getAll();
   
}
