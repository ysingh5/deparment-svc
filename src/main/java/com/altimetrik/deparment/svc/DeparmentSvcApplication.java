package com.altimetrik.deparment.svc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeparmentSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeparmentSvcApplication.class, args);
	}

}
