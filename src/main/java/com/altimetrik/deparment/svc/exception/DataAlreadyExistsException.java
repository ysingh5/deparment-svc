package com.altimetrik.deparment.svc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DataAlreadyExistsException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	private String message;
	
	public DataAlreadyExistsException(String message) {
		super(message);
	}
	
	

}
