package com.altimetrik.deparment.svc.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.altimetrik.deparment.svc.entity.Department;

public interface DepartmentRepo extends JpaRepository<Department, Long> {

   Optional<Department> findByCode(String code);
}
